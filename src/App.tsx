import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import routes from './routes';

// Create React Query Client
const queryClient = new QueryClient();

function App() {

  const content = createBrowserRouter(routes)

  return (
    <QueryClientProvider client={queryClient}>
      <RouterProvider router={content} />
    </QueryClientProvider>
  );
}

export default App;

import { useQuery, UseQueryResult } from "@tanstack/react-query"
import axios from "axios";
import { Link, useNavigate } from "react-router-dom";
import ResponseAPI from "../model/ResponseAPIType";


export default function HomePage() {

    const navigate = useNavigate();

    const { data, error, isLoading }: UseQueryResult<ResponseAPI<null>> = useQuery({
        queryKey: ["check"], queryFn: async () => {
            const res = await axios
                .get("http://localhost:8000/api/check-health",
                    {
                        headers: {
                            Accept: "application/json"
                        }
                    })
            return res.data
        }
    });

    const logout = () => {
        navigate('/login');
        localStorage.removeItem('user');
    }

    if (isLoading) return (
        <>
            <p>Loading Data....</p>
        </>
    )

    if (axios.isAxiosError(error)) return (
        <>
            <p>{"Error is occured: " + error.message}</p>
        </>
    )

    return (
        <div>
            <h1>HomePage</h1>
            <p>Home Content</p>
            <p>{data ? data.meta.status : "No Data"}</p>
            <button onClick={logout}>Logout</button>
            <Link to={'/login'}>Login Page</Link>
        </div>
    )
}
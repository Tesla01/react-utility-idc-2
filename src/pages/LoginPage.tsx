import { useMutation } from "@tanstack/react-query";
import { AxiosError } from "axios";
import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { ErrorResponse } from "../model/AuxType";
import ResponseAPI from "../model/ResponseAPIType";
import { loginUser } from "../service/AuthService";

export default function LoginPage() {
    const [error, setError] = useState<null | string>(null);
    const [nip, setNip] = useState('');
    const [password, setPassword] = useState('');

    //TODO : Try useRef for Form
    // const nip = useRef<null | HTMLInputElement>(null);
    // const password = useRef<null | HTMLInputElement>(null);

    const navigate = useNavigate();
    useEffect(() => {
        const checkUser = localStorage.getItem("user")

        if (checkUser !== null) navigate('/', { replace: true })
    });

    const mutation = useMutation({
        mutationFn: loginUser,
        onSuccess: async (data) => {
            localStorage.setItem("user", JSON.stringify(data.data?.user));
            localStorage.setItem("token", JSON.stringify(data.data?.access_token))
            navigate('/')
        },
        onError: async (error: AxiosError<ResponseAPI<ErrorResponse>>) => {
            if (error.code === "ERR_NETWORK") {
                console.log(error.code)
                setError(`${error.message}`)
                throw new Error(`${error.message}`)
            }
            console.log(error.code)
            setError(`${error.response?.data?.data.message}`)
            throw new Error(`${error.response?.data?.data.message}`)
        }
    })

    return (
        <div>
            <h1>Login Page</h1>
            <p>Login Content</p>
            {error === null ? null : <p>{"Error is occured: " + error}</p>}
            <form onSubmit={(e: React.FormEvent) => {
                e.preventDefault()
                mutation.mutate({ nip, password })
            }} onChange={() => setError(null)}>
                <input placeholder="nip" type="text" name="nip" id="nip" onChange={(e) => setNip(e.target.value)} />
                <input placeholder="password" type="text" name="password" id="password" onChange={(e) => setPassword(e.target.value)} />
                <button type="submit">Submit</button>
            </form>
        </div>
    )
}
import { useQuery, UseQueryResult } from "@tanstack/react-query";
import { Department, Plant } from "../model/AuxType";
import ResponseAPI from "../model/ResponseAPIType";
import User, { UserRole, UserType } from "../model/UserType";
import { getListDepartment, getListPlant } from "../service/AuxService";
import { getListUser, getListUserRole, getListUserType } from "../service/UserService";
import _ from "lodash";
import { useEffect } from "react";
import { useNavigate } from "react-router-dom";

export default function AdminPage() {

    const navigate = useNavigate();

    useEffect(() => {
        let checkUser = localStorage.getItem("user")
        const user: User = checkUser != null ? JSON.parse(checkUser) : {}
        if (user.user_role !== "master") navigate('/401')
    })

    // Get List User
    const listUser: UseQueryResult<ResponseAPI<User[]>> = useQuery({
        queryKey: ["list-user"], queryFn: getListUser
    });

    //Get List User Type
    const listUserType: UseQueryResult<ResponseAPI<UserType[]>> = useQuery({
        queryKey: ["list-user-type"], queryFn: getListUserType
    });

    //Get List User Role
    const listUserRole: UseQueryResult<ResponseAPI<UserRole[]>> = useQuery({
        queryKey: ["list-user-role"], queryFn: getListUserRole
    });

    //Get List Department
    const listDepartment: UseQueryResult<ResponseAPI<Department[]>> = useQuery({
        queryKey: ["list-department"], queryFn: getListDepartment
    });

    //Get List Plant
    const listPlant: UseQueryResult<ResponseAPI<Plant[]>> = useQuery({
        queryKey: ["list-plant"], queryFn: getListPlant
    });



    return (
        <>
            {listUser.isLoading ? <p>Loading Data</p> : null}
            {listUser.error ? <p>Error</p> : null}
            {listUser.data?.data.map((user) => (
                <div key={`${user.id}`} style={{ display: "flex", justifyContent: "space-around" }}>
                    <p>{user.nip}</p>
                    <p>{user.name}</p>
                    <p>{user.department}</p>
                </div>
            ))}

            <div>
                <form>
                    <input type="text" name="nip" id="nip" />

                    <button type="submit">Update</button>
                </form>
            </div>

            <div>
                input:nioo
                <select name="user-type" id="user-type">
                    {listUserType.data?.data.map((v) => (
                        <option key={`${v.id}`} value={`${v.id}`}>
                            {_.upperFirst(v.user_type)}
                        </option>
                    ))}
                </select>
            </div>

            <div>
                <select name="user-role" id="user-role">
                    {listUserRole.data?.data.map((v) => (
                        <option key={`${v.id}`} value={`${v.id}`} >
                            {_.upperFirst(v.user_role)}
                        </option>
                    ))}
                </select>
            </div>

            <div>
                <select name="department" id="department">
                    {listDepartment.data?.data.map((v) => (
                        <option key={`${v.id}`} value={`${v.id}`} >
                            {_.upperFirst(v.department)}
                        </option>
                    ))}
                </select>
            </div>

            <div>
                <select name="plant" id="plant">
                    {listPlant.data?.data.map((v) => (
                        <option key={`${v.id}`} value={`${v.id}`} >
                            {_.toUpper(v.plant)}
                        </option>
                    ))}
                </select>
            </div>
        </>
    )
}
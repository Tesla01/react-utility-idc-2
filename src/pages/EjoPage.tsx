import { Avatar, Button, Dialog, DialogTitle, List, ListItem, ListItemAvatar, ListItemText } from "@mui/material";
import { useQuery, UseQueryResult } from "@tanstack/react-query";
import { ReportEjo } from "../model/ReportType";
import ResponseAPI from "../model/ResponseAPIType";
import { getListEjo } from "../service/ReportService";

import PersonIcon from '@mui/icons-material/Person';
import AddIcon from '@mui/icons-material/Add';
import { blue } from '@mui/material/colors';
import { useState } from "react";
import DetailEjoDialog from "../component/ejo/DetailEjoDialog";

export default function EjoPage() {

    // Get List User
    const listEjo: UseQueryResult<ResponseAPI<ReportEjo[]>> = useQuery({
        queryKey: ["list-all-ejo"], queryFn: getListEjo
    });

    const [open, setOpen] = useState(false);
    const [openDetail, setOpenDetail] = useState(false);
    const [selectedEjo, setSelectedEjo] = useState<ReportEjo | null>(null);

    const [selectedValue, setSelectedValue] = useState(emails[1]);

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClickOpenDetail = (ejo: ReportEjo) => {
        setOpenDetail(true);
        setSelectedEjo(ejo);
    }

    const handleClose = (value: string) => {
        setOpen(false);
        setSelectedValue(value);
    };

    const handleClosedetail = () => {
        setOpenDetail(false);
    }

    return (
        <>
            <h1>EJO Page</h1>
            {listEjo.isLoading && <p>Loading.....</p>}
            {listEjo.error && console.log(listEjo.data?.meta.message)}
            <List>
                {listEjo.data?.data.map((ejo) => (
                    <ListItem>
                        <ListItemText key={`${ejo.id}`}>{ejo.wo_number}</ListItemText>
                        <Button variant="contained" onClick={() => {
                            handleClickOpenDetail(ejo)
                        }}>
                            Detail
                        </Button>
                    </ListItem>
                )
                )}
            </List>

            <Button variant="outlined" onClick={handleClickOpen}>
                Open simple dialog
            </Button>
            <SimpleDialog
                selectedValue={selectedValue}
                open={open}
                onClose={handleClose}
            />

            <DetailEjoDialog
                open={openDetail}
                onClose={handleClosedetail}
                ejo={selectedEjo}
            />
        </>
    )
}

const emails = ['username@gmail.com', 'user02@gmail.com'];
export interface SimpleDialogProps {
    open: boolean;
    selectedValue: string;
    onClose: (value: string) => void;
}

function SimpleDialog(props: SimpleDialogProps) {
    const { onClose, selectedValue, open } = props;

    const handleClose = () => {
        onClose(selectedValue);
    };

    const handleListItemClick = (value: string) => {
        onClose(value);
    };

    return (
        <Dialog onClose={handleClose} open={open}>
            <DialogTitle>Set backup account</DialogTitle>
            <List sx={{ pt: 0 }}>
                {emails.map((email) => (
                    <ListItem button onClick={() => handleListItemClick(email)} key={email}>
                        <ListItemAvatar>
                            <Avatar sx={{ bgcolor: blue[100], color: blue[600] }}>
                                <PersonIcon />
                            </Avatar>
                        </ListItemAvatar>
                        <ListItemText primary={email} />
                    </ListItem>
                ))}
                <ListItem autoFocus button onClick={() => handleListItemClick('addAccount')}>
                    <ListItemAvatar>
                        <Avatar>
                            <AddIcon />
                        </Avatar>
                    </ListItemAvatar>
                    <ListItemText primary="Add account" />
                </ListItem>
            </List>
        </Dialog>
    );
}
import React from "react";
import { Navigate } from "react-router-dom";

const ProtectedRoute = ({ component }) => {
  const checkUser = localStorage.getItem("user");

  if (checkUser === null) return <Navigate to={"/login"} />;

  return component;
};

export default ProtectedRoute;

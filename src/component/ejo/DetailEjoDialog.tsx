import { Dialog } from "@mui/material";
import { ReportEjo } from "../../model/ReportType";

export interface EjoDetailDialogProps {
    open: boolean;
    ejo: ReportEjo | null;
    onClose: () => void
}


const DetailEjoDialog = (props: EjoDetailDialogProps) => {
    const { onClose, ejo, open } = props;

    const handleClose = () => {
        onClose()
    }

    return (
        <Dialog onClose={handleClose} open={open}>
            {
                ejo !== null && Object.keys(ejo).map(key => (
                    <li key={key}>
                        {`${key}`}
                    </li>
                ))
            }
        </Dialog>
    )
}

export default DetailEjoDialog;
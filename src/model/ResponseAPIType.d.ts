type MetaData = {
    code: Number,
    status: string,
    message: string
}

export default interface ResponseAPI<T> {
    meta: MetaData,
    data: T
}
export default interface User {
    id: Number,
    name: string,
    nip: string,
    mobile_phone: string | Number,
    email: string,
    profile_photo_path: string,
    access: string[],
    user_type_id: Number,
    user_role_id: Number,
    deleted_at?: string | Date,
    created_at: string | Date,
    updated_at: string | Date,
    department: string,
    user_type: string,
    user_role: string
}

export type UserType = {
    id: Number,
    user_type: string
}

export type UserRole = {
    id: Number,
    user_role: string
}
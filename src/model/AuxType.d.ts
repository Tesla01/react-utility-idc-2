type Department = {
    id: Number,
    department: string
}

type Plant = {
    id: Number,
    plant: string
}

type ErrorResponse = {
    message: string,
    error: any
}

export { ErrorResponse, Department, Plant }
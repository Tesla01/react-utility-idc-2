import axios from "axios";

type InputAuth = {
  nip: string;
  password: string;
};

const loginUser = async (input: InputAuth) => {
  const res = await axios.post("http://localhost:8000/api/v1/login", input, {
    headers: {
      Accept: "application/json",
    },
  });
  return res.data;
};

const checkValidUserToken = async () => {
  const token = localStorage.getItem("token");
  const res = await axios.get("http://localhost:8000/api/v1/check-valid", {
    headers: {
      Accept: "application/json",
      Authorization: token,
    },
  });
  return res.data;
};

export { loginUser, checkValidUserToken };

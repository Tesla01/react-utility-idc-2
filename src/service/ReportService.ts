import axios, { AxiosResponse } from "axios";
import { ReportEjo } from "../model/ReportType";
import ResponseAPI from "../model/ResponseAPIType";


const { REACT_APP_API_URL } = process.env

const getListEjo = async () => {
    const res: AxiosResponse<ResponseAPI<ReportEjo[]>> = await axios.get(`${REACT_APP_API_URL}/v1/list/ejo/all`,
        {
            headers: {
                Accept: "application/json"
            }
        });
    return res.data;
}


export { getListEjo }
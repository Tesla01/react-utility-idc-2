import axios, { AxiosResponse } from "axios";
import { Department, Plant } from "../model/AuxType";
import ResponseAPI from "../model/ResponseAPIType";


const { REACT_APP_API_URL } = process.env

const getListDepartment = async () => {
    const res: AxiosResponse<ResponseAPI<Department[]>> = await axios.get(`${REACT_APP_API_URL}/v1/list/department`,
        {
            headers: {
                Accept: "application/json"
            }
        });
    return res.data;
}

const getListPlant = async () => {
    const res: AxiosResponse<ResponseAPI<Plant[]>> = await axios.get(`${REACT_APP_API_URL}/v1/list/plant`,
        {
            headers: {
                Accept: "application/json"
            }
        });
    return res.data;
}

export { getListDepartment, getListPlant }
import axios, { AxiosResponse } from "axios"
import ResponseAPI from "../model/ResponseAPIType"
import { UserRole, UserType } from "../model/UserType"

type updateUserInput = {
    nip: string,
    name: string
}


const { REACT_APP_API_URL } = process.env

const getListUser = async () => {
    const res = await axios
        .get(`${REACT_APP_API_URL}/v1/list/user`,
            {
                headers: {
                    Accept: "application/json"
                }
            });
    return res.data
}

const updateUser = (userInput: updateUserInput) => {

}

const getListUserType = async () => {
    const res: AxiosResponse<ResponseAPI<UserType[]>> = await axios.get(`${REACT_APP_API_URL}/v1/user/types`,
        {
            headers: {
                Accept: "application/json"
            }
        });
    return res.data;
}

const getListUserRole = async () => {
    const res: AxiosResponse<ResponseAPI<UserRole[]>> = await axios.get(`${REACT_APP_API_URL}/v1/user/roles`,
        {
            headers: {
                Accept: "application/json"
            }
        });
    return res.data;
}

export { getListUser, updateUser, getListUserType, getListUserRole }
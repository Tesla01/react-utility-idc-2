import { Navigate, RouteObject } from 'react-router-dom';
import ProtectedRoute from './component/ProtectedRoute';
import MainLayout from './layout/MainLayout';
import AdminPage from './pages/AdminPage';
import EjoPage from './pages/EjoPage';
import HomePage from './pages/HomePage';
import LoginPage from './pages/LoginPage';
import NotFound from './pages/NotFound';
import Unauthorized from './pages/Unauthorized';

let routes: RouteObject[] = [
    {
        path: '/',
        element: <ProtectedRoute component={< MainLayout />} />,
        children: [
            {
                path: '/',
                element: <ProtectedRoute component={< HomePage />} />,
            },
            {
                path: '/admin',
                element: <ProtectedRoute component={< AdminPage />} />,
            },
            {
                path: '/ejo',
                element: <ProtectedRoute component={< EjoPage />} />,
            },

        ]
    },
    {
        path: '/login',
        element: <LoginPage />,
    },
    {
        path: '/404',
        element: <NotFound />
    },
    {
        path: '/401',
        element: <Unauthorized />
    },
    {
        path: '*',
        element: <Navigate to={'/404'} />,
    }
]

export default routes;
import { Container } from "@mui/material";
import { Outlet } from "react-router-dom";
import MainAppBar from "../component/MainAppBar";

export default function MainLayout() {

    return (
        <>
            <MainAppBar />
            <Container>
                <Outlet />
            </Container>
        </>
    )
}